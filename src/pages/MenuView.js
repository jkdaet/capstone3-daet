import { useState, useEffect, useContext } from "react";
import {Link, useParams, useNavigate } from "react-router-dom";

import Swal from "sweetalert2";
import { Container, Card, Button, Row, Col, Form } from "react-bootstrap"

import UserContext from "../UserContext"

export default function MenuView(){

    // Checking if user is admin or not.
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [amount, setAmount] = useState(0);
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);

    const addToCart = (productId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/addToCart`, {
            method: "POST",
            headers:{
                "Content-Type":"application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                productId: productId,
                amount: amount
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true){
                Swal.fire({
                    title: "Successfully placed an order!",
                    icon: "success",
                    text: "You have successfully ordered this cake."
                })
                navigate("/products/menu");
            }
            else{
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })
            }
        })
    }

    useEffect(() => {
        console.log(productId);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setStocks(data.stocks);
        })
    }, [productId])

    return(
        <Container className="mt-5">
			<Row>
				<Col className = "p-0 pt-3 mt-5" xs={6}>
					<Card className="text-white p-0 mt-0 start-50">
						<Card.Body className="cardBg cardBorder text-center mb-2">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Form.Group className="mb-3" controlId="mobileNo">
                                <Form.Label>Amount</Form.Label>
                                <Form.Control type="number" placeholder="Amount" value={amount} onChange={e => setAmount(e.target.value)} min ="0" max={stocks} />
                            </Form.Group>
                            <div className="d-grid gap-2">
							    {
                                    (user.id !== null)
                                    ?
                                    <Button variant="danger"  size="lg" onClick={() => addToCart(productId)} xs={8}>Order</Button>
                                    :
                                    <Button as={Link} to="/login"  variant="danger" size="lg" xs={8}>Log in to order</Button>
                                }
                            </div>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
    )
}