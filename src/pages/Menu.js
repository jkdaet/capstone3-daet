import {useEffect, useState, useContext} from "react";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";

import MenuCard from "../components/MenuCard";

export default function Menu(){

    const [products, setProducts] = useState([]);

    const {user} = useContext(UserContext)

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/menuActive`)
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setProducts(data.map(product => {
                return(
                    <MenuCard key = {product._id} menuProp = {product} />
                )
            }))
        })
    }, [])
    

    return(
        (user.isAdmin)
        ?
            <Navigate to="/admin" />
        :
        <>
            <h1 className = "my-5 text-center mb-0">Menu</h1>
            {products}
        </>
    )
}