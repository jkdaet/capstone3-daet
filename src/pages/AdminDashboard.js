import { useContext, useState, useEffect } from "react";
import {Table, Button, Form, Modal} from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";

import UserContext from "../UserContext";

export default function AdminDashboard(){

    // To validate the user role.
    const {user} = useContext(UserContext);

    // Create allProducts State to contain the courses from the database.
    const [allProducts, setAllProducts] = useState([]);

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [stocks, setStocks] = useState(0);
    const [prodId, setProdId] = useState("");


    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/products/menu`, {
            headers:{
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllProducts(data.map(product => {
                return (
                    <tr key={product._id}>
                        <td>{product._id}</td>
                        <td>{product.name}</td>
                        <td>{product.description}</td>
                        <td>{product.price}</td>
                        <td>{product.stocks}</td>
                        <td>{product.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {
                                (product.isActive)
                                ?
                                    <Button variant = "danger" size ="sm"  onClick = {() => archive(product._id, product.name)}>Archive</Button>
                                :
                                    <>
                                        <Button variant = "success" size ="sm" className ="me-2" onClick = {() => unarchive(product._id, product.name)}>Unarchive</Button>
                                        <Button variant = "secondary" size = "sm" onClick ={() => updateField(product._id)}>Edit</Button>

                                    </>
                            }
                        </td>
                    </tr>
                )
            }))

        })
    }

    // Making the product inactive
    const archive = (productId, productName) => {
        console.log(productId);
        console.log(productName);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem(`token`)}`
            },
            body: JSON.stringify({
                isActive : false
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Archive Successful!",
                    icon: "success",
                    text: `${productName} is now inactive.`
                })
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Archive Unsuccessful!",
                    icon: "error",
                    text: `Something went wrong. Please try again.`
                })
            }
        })
    }

    // Making the product active
    const unarchive = (productId, productName) => {
        console.log(productId);
        console.log(productName);

        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem(`token`)}`
            },
            body: JSON.stringify({
                isActive : true
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Unarchive Successful!",
                    icon: "success",
                    text: `${productName} is now active.`
                })
                fetchData();
            }
            else{
                Swal.fire({
                    title: "Unarchive Unsuccessful!",
                    icon: "error",
                    text: `Something went wrong. Please try again.`
                })
            }
        })
    }


    //fetch all products
    useEffect(() => {
        fetchData();
    }, [])

    // Modal state for add
    const [createShow, setCreateShow] = useState(false);
    const addClose = () => setCreateShow(false);
    const addShow = () => setCreateShow(true);

    // Add product field
    // Updating Product
    const addField = (e) =>{
        e.preventDefault("")
        setName("");
        setDescription("");
        setPrice("");
        setStocks("")
        addShow();
    }

    // Add product
    const addProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem(`token`)}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                stocks: stocks
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){
                Swal.fire({
                    title: "Add product Successful!",
                    icon: "success",
                    text: `Product is now added to database.`
                })
                fetchData()
            }
            else{
                Swal.fire({
                    title: "Add product Failed!",
                    icon: "error",
                    text: `Something went wrong. Please try again.`
                })
            }
        })
    }

    // Modal state for edit
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    // Updating Product
    const updateField = (productId) =>{
        setProdId(productId);
        fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setName(data.name)
            setDescription(data.description);
            setPrice(data.price);
            setStocks(data.stocks);
        })
        console.log(productId)
        handleShow();
    }

    const updateProduct = (e,productId) => {
        e.preventDefault();
            console.log(productId)

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
                method: "PUT",
                headers: {
                    "Content-Type" : "application/json",
                    "Authorization" : `Bearer ${localStorage.getItem(`token`)}`
                },
                body: JSON.stringify({
                    name:name,
                    description:description,
                    price:price,
                    stocks:stocks
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
    
                if(data){
                    Swal.fire({
                        title: "Update Successful!",
                        icon: "success",
                        text: `Product has been updated.`
                    })
                    fetchData();
                }
                else{
                    Swal.fire({
                        title: "Update Failed!",
                        icon: "error",
                        text: `Something went wrong. Please try again.`
                    })
                }
            })
            
        }

    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if((name !== "" && description !== ""  && price !== 0 && stocks !== 0)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }
    }, [name, description, price, stocks])
    

    return(
        (user.isAdmin)
        ?
        <>
            <div className = "mt-5 mb-3 text-center">
                <h1>Admin Dashboard</h1>
                <Button variant = "primary" size = "lg" className = "mx-2 "  onClick = {(e) => addField(e)}>Add Products</Button>
                {/* <Button variant = "success" size = "lg" className = "mx-2">Show Orders</Button> */}
            </div>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>Product ID</th>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Stocks</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {allProducts}
                </tbody>
            </Table>
            {/* Modal for Add */}
            <Modal show={createShow} onHide={addClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Add Product</Modal.Title>
                        </Modal.Header>
                            <Modal.Body>
                                <Form>

                                    <Form.Group className="mb-3" controlId="name">
                                        <Form.Label>Product Name</Form.Label>
                                            <Form.Control
                                            type="text"
                                            placeholder="Product Name"
                                            value={name} onChange={e => setName(e.target.value)}
                                            />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="description">
                                        <Form.Label>Description</Form.Label>
                                            <Form.Control
                                            type="text"
                                            placeholder="Description"
                                            value={description} onChange={e => setDescription(e.target.value)}
                                            />
                                    </Form.Group>

                                    <Form.Group className="mb-3"            controlId="price">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Price"
                                            value={price} onChange={e => setPrice(e.target.value)}
                                            />
                                        </Form.Group>

                                        <Form.Group className="mb-3"            controlId="stocks">
                                        <Form.Label>Stocks</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Stocks"
                                            value={stocks} onChange={e => setStocks(e.target.value)}
                                            />
                                        </Form.Group>

                                </Form>
                            </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={addClose}>Close
                            </Button>
                            {
                            isActive
                            ?
                                <Button variant="primary"  onClick = {(e) => addProduct(e)} >
                                Add Product
                                </Button>
                            :
                                <Button variant="primary" type="submit" id="submitBtn" disabled >
                                Add Product
                                </Button>
                            }
                        </Modal.Footer>
            </Modal>

            {/* Modal for Update */}
            <Modal show={show} onHide={handleClose} centered>
                <Form onSubmit= {(e) => updateProduct(e, prodId)}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Product</Modal.Title>
                        </Modal.Header>
                            <Modal.Body>
                               

                                    <Form.Group className="mb-3" controlId="name">
                                        <Form.Label>Product Name</Form.Label>
                                            <Form.Control
                                            type="text"
                                            placeholder="Product name"
                                            value={name} onChange={e => setName(e.target.value)}
                                            />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="description">
                                        <Form.Label>Description</Form.Label>
                                            <Form.Control
                                            type="text"
                                            placeholder="Description"
                                            value={description} onChange={e => setDescription(e.target.value)}
                                            />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="price">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Price"
                                            value={price} onChange={e => setPrice(e.target.value)}
                                            />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="stocks">
                                        <Form.Label>Stocks</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Stocks Available"
                                            value={stocks} onChange={e => setStocks(e.target.value)}
                                            />
                                    </Form.Group>
                                
                            </Modal.Body>

                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleClose}>Close
                            </Button>
                            <Button variant="primary" type="submit" id="submitBtn">Save Changes
                            </Button>
                        </Modal.Footer>
                    </Form>
            </Modal>

            
        </>
        :
        <Navigate to = "/products/menu" />
        
 
        )
}
