import {Row, Col, Button, Card} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){
    console.log(data);

    const {title, content, destination, label} = data;

    return(
        <Row>
            <Card className="text-white p-0 mt-0 g-0 position-absolute top-50">
                    <Card.Body className="position-absolute top-100 start-0 ms-5 mt-5 p-0 cardBg cardBorder">
                        <Col className = "p-5 mt-0">
                            <h1>{title}</h1>
                            <p>{content}</p>
                            <Button as={Link} to={destination} variant = "danger">{label}</Button>
                        </Col>
                    </Card.Body>
            </Card>
        </Row>
    )
}