import {Link} from "react-router-dom"

import {Row, Col, Card, Button, CardGroup} from "react-bootstrap";

export default function MenuCard({menuProp}){

    // Deconstruct properties
    const {_id, name, description, price, stocks} = menuProp;

    return (
        <>
        <CardGroup>
        {/* <Row className="mt-0 mb-2 p-0 cardBg2"> */}
            <Col className = "p-0 pt-3 mt-0" xs={6}>
                <Card className="cardHighlight p-1 text-white start-50">
                    <Card.Body className="cardBg text-center">
                        <Card.Title>
                            {name}
                        </Card.Title>
                        <Card.Subtitle>
                            Description:
                        </Card.Subtitle>
                        <Card.Text>
                            {description}
                        </Card.Text>
                        <Card.Subtitle>
                            Price:
                        </Card.Subtitle>
                        <Card.Text>
                            {price}
                        </Card.Text>
                        <Card.Text>
                            Stocks : {stocks}
                        </Card.Text>
                        <Button as={Link} to={`/products/${_id}`} variant = "danger">Details</Button>
                    </Card.Body>
                </Card>
            </Col>
        {/* </Row> */}
    </CardGroup>
    </>
    )
}