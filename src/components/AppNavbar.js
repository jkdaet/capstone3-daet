import { Link } from "react-router-dom";
import {useContext} from "react";
import UserContext from "../UserContext"

import React from "react"


import {Navbar, Nav, Container} from "react-bootstrap";

export default function AppNavbar(){

    const {user} = useContext(UserContext);

    return (
    <>
    <Navbar bg="dark" variant="dark" fixed="top">
        <Container>
          <Navbar.Brand as={Link} to="/">
          <img
              src="/cake.png"
              width="30"
              height="30"
              className="d-inline-block align-top me-3"
              alt="JCakes Logo"
            />
            JCakes
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto" defaultActiveKey="/">
              <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
              {
              (user.isAdmin)
                ?
                <Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>
                :
                <Nav.Link as={Link} to="/products/menu" eventKey="/products/menu">Menu</Nav.Link>
              }

              {
                (user.id !== null)
                ?
                  <Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
                :
                  <>
                    <Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
                    <Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
                  </>
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
    )
}